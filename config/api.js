const ApiRootUrl = 'https://www.hiolabs.com/api/';
module.exports = {
    // 首页
    IndexUrl: ApiRootUrl + 'index/appInfo', // 首页所有数据
    // 分类
    CatalogList: ApiRootUrl + 'catalog/index', // 分类左侧数据
    GetCurrentList: ApiRootUrl + 'catalog/currentlist', // 分类右侧数据
    // 详情
    GoodsDetail: ApiRootUrl + 'goods/detail', //获得商品的详情
}