// app.js
App({
  data:{
    userInfo: {},
    cartList: [],
    price: 0
  },

  addCount(id){
    const list = this.data.cartList
    const index = list.findIndex(val => val.id === id)
    list[index].count ++
    console.log(list)
    this.totalPrice()
  },

  reduceCount(id){
    const list = this.data.cartList
    const index = list.findIndex(val => val.id === id)
    if (list[index].count>1) {
      list[index].count --
    } else {
      list[index].count = 1
    }
    this.totalPrice()
  },

  totalPrice() {
    const list = this.data.cartList
    if(list.length){
      this.data.price = 0
      list.forEach(item => {
        if(item.checked){
          this.data.price += item.min_agency_price * item.count
        }
      })
    }
  },

  changeItemChecked(id){
    const list = this.data.cartList
    const index = list.findIndex(val => val.id === id)
    list[index].checked = !list[index].checked
    this.totalPrice()
  },

  handleCheckAll(flag) {
    const list = this.data.cartList
    list.forEach(item => {
      item.checked = flag
    })
    this.totalPrice()
  },



  onLaunch() {
    // 展示本地存储能力
    const logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
      }
    })
  },
  globalData: {
    userInfo: null,
    footprintList: []
  }
})
