// pages/detail/index.js
import { GoodsDetail } from '../../config/api'
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    gallery: [], //轮播图数据
    info: {}, // 商品信息
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    circular: true,
    interval: 2000,
    duration: 500,
    previousMargin: 0,
    nextMargin: 0,
  },
  toHome() {
    wx.switchTab({
      url: '/pages/index/index',
    })
  },
  toCart() {
    wx.switchTab({
      url: '/pages/cart/index',
    })
  },

  addCar(){
    if (app.data.userInfo.nickName){
      const flag = app.data.cartList.findIndex(val => val.id === this.data.info.id)
      if (flag >= 0) {
        app.data.cartList[flag].count ++
      } else {
        this.data.info.checked = true
        this.data.info.count = 1
        app.data.cartList.push(this.data.info)
      }
    } else {
      wx.navigateTo({
        url: '/pages/person/login/index',
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getDetailData(options.id)
  },
  
  getDetailData(id) {
    wx.request({
      url: GoodsDetail + '?id=' + id,
      success: res => {
        this.setData({
          gallery: res.data.data.gallery,
          info: res.data.data.info
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})