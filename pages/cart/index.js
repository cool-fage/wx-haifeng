// pages/cart/index.js
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    totalPrice: 0,
    checkbox: true,
    checkall: true
  },

  toIndexPage(){
    wx.switchTab({
      url: '/pages/category/index',
    })
  },

  addCount(e){
    const id = e.currentTarget.dataset.id
    app.addCount(id)
    this.getList()    
  },
  reduceCount(e){
    const id = e.currentTarget.dataset.id
    app.reduceCount(id)
    this.getList()
  },
  handleIptItem(e) {
    const id = e.currentTarget.dataset.id
    app.changeItemChecked(id)
    this.getList()
    const flag = this.data.list.every(item => item.checked)
    this.setData({
      checkall:flag
    })
  },

  checkAll() {
    this.setData({
      checkall: !this.data.checkall
    })
    app.handleCheckAll(this.data.checkall)
    this.getList()
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  
  getList(){
    if(app.data.cartList.length){
      this.setData({
        list: app.data.cartList,
        totalPrice: app.data.price.toFixed(2)
      })
    }
   },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getList()
    app.totalPrice()
    if(this.data.list.length){
      let price = 0
      this.data.list.forEach(item => {
        if(item.checked){
          price += item.min_agency_price * item.count
        }
      })
      this.setData({
        totalPrice: price.toFixed(2)
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})