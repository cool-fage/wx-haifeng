// index.js
// 获取应用实例
const app = getApp()
import { IndexUrl } from '../../config/api'

Page({
  data: {
    background: [], // 轮播图数据
    indicatorDots: true,
    vertical: false,
    autoplay: true,
    circular: true,
    interval: 2000,
    duration: 500,
    previousMargin: 0,
    nextMargin: 0,
    channel: [], // icon页数据
    categoryList: [], // 商品展示数据
  },
  changeProperty: function (e) {
    var propertyName = e.currentTarget.dataset.propertyName
    var newData = {}
    newData[propertyName] = e.detail.value
    this.setData(newData)
  },
  changeIndicatorDots: function (e) {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },
  changeAutoplay: function (e) {
    this.setData({
      autoplay: !this.data.autoplay
    })
  },
  intervalChange: function (e) {
    this.setData({
      interval: e.detail.value
    })
  },
  durationChange: function (e) {
    this.setData({
      duration: e.detail.value
    })
  },
  /**
    * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.request({
      url: IndexUrl,
      success: res => {
        this.setData({
          background: [...res.data.data.banner],
          channel: [...res.data.data.channel],
          categoryList: [...res.data.data.categoryList]
        })
      }
    })
  },
})