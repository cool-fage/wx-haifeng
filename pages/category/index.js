// pages/category/index.js
const app = getApp()
import { CatalogList, GetCurrentList } from '../../config/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categoryList: [], // 左侧列表数据
    // tabInd: 0,
    // tabId: 1036024,
    tabId: 0,
    id: 0,
    page: 1,
    size: 8,
    list: [], // 右侧列表数据
  },

  handleChangeIndex(e){
    this.setData({
      // tabInd: e.currentTarget.dataset.idx,
      tabId: e.currentTarget.dataset.id
    })
    this.getList()
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  getList() {
    const that = this
    wx.request({
      url: GetCurrentList,
      method: 'POST',
      data:{
        id: that.data.tabId,
        page: 1,
        size: 100,
      },
      success: res => {
        that.setData({
          list:res.data.data.data
        })
      }
    })
  },

  handleToDetail(e) {
    wx.navigateTo({
      url: `/pages/detail/index?id=${e.currentTarget.dataset.id}`,
    })
    let flag = app.globalData.footprintList.findIndex(item => item.id === e.currentTarget.dataset.id)
    if(flag === -1) {
      app.globalData.footprintList.push(e.currentTarget.dataset)
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    wx.request({
      url: CatalogList,
      success: res => {
        this.setData({
          categoryList: res.data.data.categoryList
        })
      }
    })
    this.getList()
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})